<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MoviesController@index');
Route::get('/seances/{movie_id}', 'SeancesController@show')->name('seances.show');


Route::get('/show_places/{seance_id}', 'PlacesController@show')->name('places.show');
Route::get('/places/get_busy/{seance_id}', 'PlacesController@buzy')->name('places.busy');

Route::post('/places/perform_reservation/', 'PlacesController@reservation')->name('places.reservation');
Route::post('/places/cancel_reservation/', 'PlacesController@cancelReservation')->name('places.cancelReservation');
