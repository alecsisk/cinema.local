<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {

        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seance_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('row');
            $table->integer('column');
            $table->unique(['seance_id', 'row', 'column']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('seance_id')
                ->references('id')->on('seances')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down () {
        Schema::dropIfExists('places');
    }
}
