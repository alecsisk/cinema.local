<?php

use Illuminate\Database\Seeder;

class SeancesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        // получаем все фильмы
        $movies = App\Movie::all();

        // получаем все залы
        $halls = App\Hall::all();

        /**
         * генерируем сеансы
         */
        for ($i = 0; $i < 60; $i++) {

            // получаем id случайного фильма
            $movie_id = $movies->random(1)->first()->id;

            // получаем id случайного зала
            $hall_id = $halls->random(1)->first()->id;

            // генерируем сеанс
            factory(App\Seance::class, 'seance')->create([
                'movie_id' => $movie_id,
                'hall_id' => $hall_id
            ]);
        }
    }
}
