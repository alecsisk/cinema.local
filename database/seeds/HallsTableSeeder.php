<?php

use Illuminate\Database\Seeder;

class HallsTableSeeder extends Seeder {


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        /**
         * создаем залы
         */
        factory(App\Hall::class, 'hall', 5)->create();
    }
}
