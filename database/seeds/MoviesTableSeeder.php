<?php

use Illuminate\Database\Seeder;

class MoviesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        /*
        // Populate roles
        factory(App\Role::class, 20)->create();

        // Populate users
        factory(App\User::class, 50)->create();

        // Get all the roles attaching up to 3 random roles to each user
        $roles = App\Role::all();

        // Populate the pivot table
        App\User::all()->each(function ($user) use ($roles) {
            $user->roles()->attach(
                $roles->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
        */


        /*
        $movie = factory(App\Movie::class, 'movie')->create();
        $hall = factory(App\Hall::class, 'hall')->create();

        factory(App\Seance::class, 'seance', 5)->create(['movie_id' => $movie->id, 'hall_id' => $hall->id]);
        */


        /**
         * создаем фильмы
         */
        factory(App\Movie::class, 'movie', 10)->create();
    }
}
