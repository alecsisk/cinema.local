<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        /**
         * создаем 5 пользователей
         */
        factory(App\User::class, 'user', 5)->create();

        /**
         * и одного админа
         */
        factory(App\User::class, 'admin', 1)->create();
    }
}
