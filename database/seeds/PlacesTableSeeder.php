<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder {


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run () {

        // получаем всех пользователей
        $users = App\User::all();

        // и все сеансы с залами
        $seances = App\Seance::with('hall')->get();


        /**
         * занимаем места
         */
        for ($i = 0; $i < 400; $i++) {

            // получаем случайный сеанс
            $seance = $seances->random(1)->first();

            // получаем id случайного пользователя
            $user_id = $users->random(1)->first()->id;

            // место, которое занимаем
            $row = rand(0, $seance->hall()->first()->rows);
            $column = rand(0, $seance->hall()->first()->columns);

            // смотрим, занято-ли место
            $result = App\Place::where('seance_id', '=', $seance->id)
                ->where('row', '=', $row)
                ->where('column', '=', $column)
                ->exists();

            // если место никем не занято
            if ($result === false) {

                // занимаем место
                factory(App\Place::class, 'place')->create([
                    'user_id' => $user_id,
                    'seance_id' => $seance->id,
                    'row' => $row,
                    'column' => $column,
                ]);
            }
        }
    }
}
