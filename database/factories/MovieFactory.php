<?php

use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


$factory->defineAs(App\Movie::class, 'movie', function (Faker $faker) {

    return [
        'name' => $faker->unique()->word,
        'description' => $faker->text(200),
        'image_url' => $faker->image('public/images/movies', 300, 450, null, false),
    ];
});


$factory->defineAs(App\Hall::class, 'hall', function (Faker $faker) {

    return [
        'name' => 'Зал ' . $faker->unique()->word,
        'rows' => rand(10, 15),
        'columns' => rand(8, 12),
    ];
});


$factory->defineAs(App\Seance::class, 'seance', function (Faker $faker) {

    return [
        'time' => Carbon::now()->addHours(rand(1, 24 * 5)),
    ];
});


$factory->defineAs(App\Place::class, 'place', function (Faker $faker) {

    return [

    ];
});

