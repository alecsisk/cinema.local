<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest () {

        $user = new User([
            'id' => 1,
            'name' => 'test'
        ]);

        $this->be($user);

        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
