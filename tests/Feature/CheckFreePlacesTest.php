<?php

namespace Tests\Feature;

use App\Place;
use Tests\TestCase;

class CheckFreePlacesTest extends TestCase {
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCheckFreePlaces () {

        // все сеансы с залами
        $seances = \App\Seance::with('hall')->get();

        // получаем случайный сеанс
        $seance = $seances->random(1)->first();

        // получаем кол-во занятых мест на этот сеанс
        $busyPlaces = Place::ofSeance($seance->id)->count();

        // всего мест
        $placesCount = $seance->hall()->first()->rows * $seance->hall()->first()->columns;

        // свободных мест должно быть больше 0
        $this->assertGreaterThan(0, $placesCount - $busyPlaces, 'not free places on seance id: ' . $seance->id);
    }
}
