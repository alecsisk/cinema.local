<?php
/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 05.09.2018
 * Time: 22:00
 */

namespace App\Http\Controllers;


use App\Http\Requests\CancelReservationPlaceRequest;
use App\Http\Requests\ReservationPlaceRequest;
use App\Place;
use App\Seance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PlacesController extends Controller {


    public function __construct () {
        $this->middleware('auth');
    }


    /**
     * получить занятые места для сеанса
     * @param $seance_id
     * @return mixed
     */
    public function buzy ($seance_id) {

        // id текущего пользователя
        $user_id = auth()->user()->id;

        // занятые места (передаем id места, если место занято текущим пользовтелем)
        return Place::ofSeance($seance_id)
            ->get(['row', 'column', DB::raw("if(user_id = {$user_id}, id, null) as id")]);
    }


    /**
     * @param $seance_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show ($seance_id) {

        // данные сеанса, фильма и кол-во мест
        $seance = Seance::with('movie')->with('hall')->find($seance_id);

        // занятые места
        $places = Place::ofSeance($seance_id)->get()->toArray();

        // возвращаем вьюшку
        return view('movies.places', compact(['seance', 'places']));
    }


    /**
     * забронировать место
     * @param ReservationPlaceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reservation (ReservationPlaceRequest $request) {

        try {

            // данные запроса
            $input = $request->all();

            // добавляем id текущего пользователя
            $input['user_id'] = auth()->user()->id;

            // бронируем место
            $place = new Place();
            $place->fill($input);
            $place->save();

            // успешно забронировали, возвращаем id
            return response()->json(['id' => $place->id], 200);

        } catch (\Throwable $e) {

            // ошибка бронирования
            return response()->json(['response' => $e->getMessage()], 401);
        }
    }


    /**
     * отмена бронирования
     * @param CancelReservationPlaceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelReservation (CancelReservationPlaceRequest $request) {

        try {

            // получаем бронивание, которое нужно отменить и сразу делаем проверку user_id
            $result = Place::where('id', '=', $request->id)
                ->where('user_id', '=', auth()->user()->id)
                ->first();

            // удаляем данные
            $result->delete();

            // возвращаем данные
            return response()->json(['response' => 'OK'], 200);

        } catch (\Throwable $e) {

            // ошибка отмены бронирования
            return response()->json(['response' => $e->getMessage()], 401);
        }
    }
}
