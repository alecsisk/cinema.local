<?php
/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 04.09.2018
 * Time: 19:26
 */

namespace App\Http\Controllers;


use App\Movie;
use App\Seance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SeancesController extends Controller {


    public function __construct () {
        $this->middleware('auth');
    }


    public function show ($movie_id) {

        // получаем данные о текущем фильме
        $movie = Movie::findOrFail($movie_id);

        // получаем данные сеансов
        $seances = Seance::ofMovie($movie_id)
            ->where('time', '>', Carbon::now()->toDateTimeString())
            ->orderBy('day_of_seance', 'asc')
            ->orderBy('hall_id', 'asc')
            ->with('hall')
            ->get(['*', DB::raw("DATE(time) as day_of_seance")]);

        // строим вьюшку
        return view('movies.seances', compact(['seances', 'movie']));
    }
}
