<?php
/**
 * Created by IntelliJ IDEA.
 * User: vision
 * Date: 04.09.2018
 * Time: 17:23
 */

namespace App\Http\Controllers;


use App\Movie;

class MoviesController extends Controller {


    public function __construct () {
        $this->middleware('auth');
    }


    public function index () {
        $movies = Movie::all();
        return view('movies.index', compact('movies'));
    }
}
