<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hall extends Model {
    public $timestamps = false;

    public function seance () {
        return $this->hasMany('App\Seance', 'seance_id', 'id');
    }
}
