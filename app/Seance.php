<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seance extends Model {
    public $timestamps = false;


    public function movie () {
        return $this->belongsTo('App\Movie', 'movie_id', 'id');
    }


    public function hall () {
        return $this->belongsTo('App\Hall', 'hall_id', 'id');
    }


    public function scopeOfMovie ($query, $movie_id) {
        return $query->where('movie_id', '=', $movie_id);
    }
}
