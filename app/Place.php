<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Place extends Model {
    public $fillable = ['seance_id', 'user_id', 'row', 'column'];
    public $timestamps = false;


    public function scopeOfSeance ($query, $seance_id) {
        return $query->where('seance_id', '=', $seance_id);
    }


    public function seance () {
        return $this->belongsTo('App\Seance', 'seance_id', 'id');
    }
}
