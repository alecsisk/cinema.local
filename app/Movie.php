<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model {
    public $timestamps = false;


    public function seances () {
        return $this->hasMany('App\Seance', 'movie_id', 'id');
    }
}
