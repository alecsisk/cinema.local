@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row flex-row">
            <div class="col-md-auto">
                <img src="{{ asset('/images/movies/' . $movie->image_url) }}" alt="{{ $movie->name }}"/>
            </div>
            <div class="col-md-8">
                <h1>{{ $movie->name }}</h1>
                <p>{{ $movie->description }}</p>
            </div>
        </div>

        @php
            $currentDay = null;
        @endphp

        @foreach($seances as $seance)

            @if ($currentDay != $seance->day_of_seance)

                <!-- если есть класс seance-day, и row который нужно закрыть -->
                @if ($currentDay != null)
                    </div></div>
                @endif

                <!-- создаем новый seance-day -->
                <div class="seance-day">

                    <!-- выводим дату сеанса -->
                    <h3>{{ \Jenssegers\Date\Date::parse($seance->day_of_seance)->format('j F Y') }}</h3>

                    @php
                        // по-умолчанию для дня не указан зал
                        $hall_id = null;
                    @endphp

                    @php
                        $currentDay = $seance->day_of_seance;
                    @endphp

            @endif

            @if ($hall_id != $seance->hall_id)

                <!-- выводим имя зала -->
                <h5 class="hall-name">{{ $seance->hall->name }}</h5>

                <!-- для вывода времени в одну строку -->
                <div class="flex-row">

                @php
                    // сохраняем hall_id
                    $hall_id = $seance->hall_id
                @endphp

            @endif

                <button type="button" data-url="{{ route('places.show', ['id' => $seance->id]) }}"
                    class="btn btn-primary seance">{{ \Jenssegers\Date\Date::parse($seance->time)->format('H:i') }}</button>
        @endforeach

        </div>

    </div>

    <script>

        $(document).ready(function () {

            $('button.seance').click(function () {
                window.location.href = $(this).data('url');
            });
        });

    </script>

@endsection
