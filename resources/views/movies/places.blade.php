@extends('layouts.app')

@section('content')


    <div class="container places-container">

        <!-- выводим имя фильма -->
        <h3>{{ $seance->movie->name }}</h3>

        <!-- имя зала и дата начала сеанса -->
        <h6>{{ $seance->hall->name . ', ' . \Jenssegers\Date\Date::parse($seance->time)->format('j F Y, H:i') }}</h6>

    @php

        // место
        $place = 1;

    @endphp

        <!-- формируем запрос для получения занятых мест -->
        <input id="url-places-busy-get" type="hidden" data-url="{{ route('places.busy', ['id' => $seance->id]) }}">

        <!-- и для бронирования -->
        <input id="url-places-reservation" type="hidden" data-seance_id="{{ $seance->id }}" data-url="{{ route('places.reservation') }}">

        <!-- для отмены бронирования -->
        <input id="url-places-cancelReservation" type="hidden" data-seance_id="{{ $seance->id }}" data-url="{{ route('places.cancelReservation') }}">


        <div id="seat-container" hidden>

            <!-- колонки -->
            @for ($i = 0; $i < $seance->hall->columns; $i++)

                <div class="seat-row-container">

                    <!-- поля -->
                @for ($b = 0; $b < $seance->hall->rows; $b++)

                    <!-- рисуем места -->
                        <button type="button" class="seat-button free" data-column="{{$i}}"
                                data-row="{{$b}}" onclick="">{{$place}}</button>

                        @php
                            $place++;
                        @endphp

                    @endfor

                </div>

            @endfor

        </div>

    </div>


    <script>

        $(document).ready(function () {

            /**
             * добавляем csrf-токен в любой запрос
             */
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            // запрашиваем занятые места
            $.ajax({
                'type': 'GET',
                'url': $('#url-places-busy-get').data('url'),
                'success': function (data) {

                    // проходимся по данным
                    for (var i = 0; i < data.length; i++) {

                        // данные занятого места
                        var busyPlace = data[i];

                        // получаем нужную кнопку
                        var button = $("button.seat-button[data-column=" + busyPlace.column + "][data-row=" + busyPlace.row + "]");

                        // место не свободно
                        button.removeClass('free');

                        // если не передается id'щник места
                        if (busyPlace.id == null) {

                            // добавляем класс что место занято не нами
                            button.addClass('busy');

                        } else {

                            // добавляем что место занято нами
                            button.addClass('my');

                            // добавляем данные id в место
                            button.data('id', busyPlace.id);
                        }
                    }

                    // показываем места, тк данные по занятым местам загрузились
                    $('#seat-container').removeAttr('hidden');

                },
                'fail': function () {
                    console.log('fail to get busy places');
                }
            });


            // для всех кнопок-мест назначаем событие по клику
            $("button.seat-button").click(function () {

                // получаем кнопку
                var button = $(this);

                // если место свободно
                if (button.hasClass('free')) {

                    console.log('free');

                    // удаляем класс, что место свободно
                    button.removeClass('free');

                    // добавляем класс, что место резирвируется (позволяет избавится от повторных запросов)
                    button.addClass('reservation');

                    // резервирем место
                    $.ajax({
                        'type': 'POST',
                        'url': $('#url-places-reservation').data('url'),
                        'data': {
                            'seance_id': $('#url-places-reservation').data('seance_id'),
                            'row': $(this).data('row'),
                            'column': $(this).data('column')
                        },
                        'success': function (data) {

                            // удаляем класс, что место резирвируется
                            button.removeClass('reservation');

                            // добавляем класс, что место зарезирвировано нами
                            button.addClass('my');

                            // добавляем id к нашей кнопке
                            button.data('id', data.id);
                        },
                        'fail': function (data) {

                            // удаляем класс, что место резирвируется
                            button.removeClass('reservation');

                            // место свободно
                            button.addClass('free');

                            // выводим логи
                            console.log('fail to reservation place: ' + data.toString());
                        }
                    });

                } else if ($(this).hasClass('my')) {

                    // удаляем класс, что место зарезирвировано нами
                    button.removeClass('my');

                    // добавляем класс, что место резирвируется (позволяет избавится от повторных запросов)
                    button.addClass('reservation');

                    // отмена резервирования места
                    $.ajax({
                        'type': 'POST',
                        'url': $('#url-places-cancelReservation').data('url'),
                        'data': {
                            'id': button.data('id')
                        },
                        'success': function (data) {

                            // удаляем класс, что место резирвируется
                            button.removeClass('reservation');

                            // добавляем класс, что место свободно
                            button.addClass('free');

                            // удаляем id у нашей кнопки
                            button.removeData('id');
                        },
                        'fail': function (data) {

                            // удаляем класс, что место резирвируется
                            button.removeClass('reservation');

                            // добавляем класс, что место зарезирвировано нами
                            button.addClass('my');

                            // выводим логи
                            console.log('fail to cancel reservation place: ' + data.toString());
                        }
                    });


                } else if ($(this).hasClass('reservation')) {

                    // место занято
                    console.log('место в данный момент резервируется')

                } else {

                    // место уже занято не нами
                    console.log('место занято не нами');
                }
            })
        });

    </script>

@endsection
