@extends('layouts.app')

@section('content')

    <div class="container">

        <h3 class="text-center">Кинотеатр "Искра"</h3>

        @foreach($movies as $movie)
            <div class="btn">
                <a href="{{ route('seances.show', ['id' => $movie->id]) }}">
                    <img src="{{ asset('/images/movies/' . $movie->image_url) }}" alt="{{ $movie->name }}"/>
                    <p>{{ $movie->name }}</p>
                </a>
            </div>
        @endforeach
    </div>

@endsection
